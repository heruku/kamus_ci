<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wilayah extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Wilayah_model','wilayah');
		// $this->load->model('Group/Group_model','group');
	}

	public function index()
	{

		$this->load->view('view_wilayah');
	}


	public function ajax_list()
	{

		$list = $this->wilayah->get_datatables();
		$data = array();
		$no = $_POST['start'];


		foreach ($list as $post) {


				$link_edit = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit('."'".$post->kode_wilayah."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';

				$link_hapus = ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$post->kode_wilayah."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';


			$no++;
			$row = array();
         	$row[] = $no;
         	$row[] = $post->kode_wilayah;
         	$row[] = $post->nama_kecamatan;
         	$row[] = $post->nama_kelurahan;
			//add html for action
			$row[] = $link_edit.$link_hapus;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->wilayah->count_all(),
						"recordsFiltered" => $this->wilayah->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->wilayah->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{

		// $this->_validate();
		$post_date = time();
		$post_date_format = date('Y-m-d h:i:s', $post_date);
      // $user = $this->ion_auth->user()->row();
		$data = array(
				'nama_barang' 		=> $this->input->post('nama_barang'),
            	'satuan_id' 		=> $this->input->post('satuan'),
				'jenis_barang_id' 	=> $this->input->post('jenis')
			);

		$insert = $this->wilayah->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$this->_validate();
		$post_date = time();
		$post_date_format = date('Y-m-d h:i:s', $post_date);

		$data = array(
			'nama_barang' 		=> $this->input->post('nama_barang'),
        	'satuan_id' 		=> $this->input->post('satuan'),
			'jenis_barang_id' 	=> $this->input->post('jenis')
		);
		
		
		$this->wilayah->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->wilayah->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}


	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('nama_barang') == '')
		{
			$data['inputerror'][] = 'nama_barang';
			$data['error_string'][] = 'Nama Barang harus diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('jenis') == '')
		{
			$data['inputerror'][] = 'jenis';
			$data['error_string'][] = 'Jenis Barang harus diisi';
			$data['status'] = FALSE;
		}

		// if($this->input->post('password') == '')
		// {
		// 	$data['inputerror'][] = 'password';
		// 	$data['error_string'][] = 'Password harus diisi';
		// 	$data['status'] = FALSE;
		// }

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}



}
